/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.bootstrapers.demo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.cafeteriausermanagement.domain.CafeteriaUser;
import eapli.ecafeteria.cafeteriausermanagement.domain.MecanographicNumber;
import eapli.ecafeteria.cafeteriausermanagement.repositories.CafeteriaUserRepository;
import eapli.ecafeteria.infrastructure.bootstrapers.TestDataConstants;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.ecafeteria.mealbooking.application.BookAMealForADayController;
import eapli.ecafeteria.mealbooking.domain.BookingToken;
import eapli.ecafeteria.mealmanagement.domain.Meal;
import eapli.ecafeteria.mealmanagement.repositories.MealRepository;
import eapli.framework.actions.Action;

/**
 *
 * @author pgsou_000
 *
 */
public class BookingBootstrapper implements Action {

    private static final Logger LOGGER = LogManager.getLogger(BookingBootstrapper.class);

    private final BookAMealForADayController controller = new BookAMealForADayController();
    private final MealRepository mealRepo = PersistenceContext.repositories().meals();
    private final CafeteriaUserRepository userRepo = PersistenceContext.repositories()
            .cafeteriaUsers();

    private final List<BookingToken> tokens = new ArrayList<>();

    public List<BookingToken> bookings() {
        return tokens;
    }

    @Override
    public boolean execute() {

        final BookingToken token = book(TestDataConstants.USER_TEST1,
                TestDataConstants.DATE_TO_BOOK);
        if (token == null) {
            return false;
        }

        tokens.add(token);

        return true;
    }

    private BookingToken book(final String number, final Calendar when) {
        final Meal meal = mealRepo.findByDay(when, TestDataConstants.MEAL_TYPE_TO_BOOK).iterator()
                .next();
        final CafeteriaUser booker = userRepo.ofIdentity(MecanographicNumber.valueOf(number))
                .orElseThrow(IllegalStateException::new);
        try {
            final BookingToken token = controller.bookMeal(meal, booker);
            LOGGER.debug("Booked meal {} for user {} with token {}", meal, booker, token);
            return token;
        } catch (final Exception e) {
            LOGGER.error("This should not happen", e);
            return null;
        }
    }
}
