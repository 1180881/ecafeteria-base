/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and
 * associated documentation files (the "Software"), to deal in the Software
 * without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom
 * the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 * PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 * FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.infrastructure.smoketests.backoffice;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishController;
import eapli.ecafeteria.dishmanagement.application.ActivateDeactivateDishTypeController;
import eapli.ecafeteria.dishmanagement.application.ChangeDishController;
import eapli.ecafeteria.dishmanagement.application.ChangeDishTypeController;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.dishmanagement.domain.DishType;
import eapli.ecafeteria.dishmanagement.domain.NutricionalInfo;
import eapli.ecafeteria.reporting.dishes.application.DishReportingController;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerCaloricCategory;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerDishType;
import eapli.framework.actions.Action;
import eapli.framework.general.domain.model.Money;
import eapli.framework.representations.builders.JsonRepresentationBuilder;
import eapli.framework.util.StringFormater;
import eapli.framework.validations.Invariants;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
public class DishManagementSmokeTester implements Action {
    private static final Logger LOGGER = LogManager.getLogger(DishManagementSmokeTester.class);

    final ChangeDishTypeController changeDishTypeController = new ChangeDishTypeController();
    final ChangeDishController changeDishController = new ChangeDishController();
    final DishTypeCRUDSmokeTester crudTester = new DishTypeCRUDSmokeTester();
    final ActivateDeactivateDishController activateDishController = new ActivateDeactivateDishController();
    final DishReportingController dishReportingController = new DishReportingController();
    final ActivateDeactivateDishTypeController activateDishTypeController = new ActivateDeactivateDishTypeController();

    @Override
    public boolean execute() {
        testDishTypeCRUD();
        testActivateDeactivateDishType();
        testChangeDishType();

        testActivateDeactivateDish();
        testChangeDish();
        testReportingDish();

        testDishJsonRepresentation();

        // nothing else to do
        return true;
    }

    private void testDishJsonRepresentation() {
        final Iterable<Dish> l = changeDishController.allDishes();
        Invariants.nonNull(l);

        final Dish dish = l.iterator().next();

        final JsonRepresentationBuilder builder = new JsonRepresentationBuilder();
        final String json = dish.buildRepresentation(builder);
        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("-- DISH JSON --");
            LOGGER.info(json);
            LOGGER.info("-- DISH Pretty JSON --");
            LOGGER.info(StringFormater.prettyformatJson(json));
        }
    }

    private void testDishTypeCRUD() {
        crudTester.testDishTypeCRUD();
    }

    private void testChangeDishType() {
        final Iterable<DishType> l = changeDishTypeController.dishTypes();
        Invariants.nonNull(l);

        DishType dt = l.iterator().next();

        final String newDescription = dt.description() + " (edited)";
        dt = changeDishTypeController.changeDishType(dt, newDescription);
        Invariants.ensure(dt.description().equals(newDescription));

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("»»» Changed description of {} to {}", dt.identity(), dt.description());
        }
    }

    private void testActivateDeactivateDish() {
        final Iterable<Dish> l = activateDishController.allDishes();
        Invariants.nonNull(l);

        Dish dish = l.iterator().next();

        final boolean oldStatus = dish.isActive();

        dish = activateDishController.changeDishState(dish);
        Invariants.ensure(dish.isActive() == !oldStatus);

        LOGGER.info("»»» Changed status of {} to {}", dish, dish.isActive());
    }

    private void testChangeDish() {
        final Iterable<Dish> l = changeDishController.allDishes();
        Invariants.nonNull(l);

        Dish dish = l.iterator().next();

        // change price
        final Money oldPrice = dish.currentPrice();
        final Money newPrice = oldPrice.add(Money.euros(1));

        dish = changeDishController.changeDishPrice(dish, newPrice);
        Invariants.ensure(dish.currentPrice().equals(newPrice));

        LOGGER.info("»»» Changed price of {} to {}", dish, dish.currentPrice());

        dish.nutricionalInfo();
        final NutricionalInfo newInfo = new NutricionalInfo(10, 2);

        dish = changeDishController.changeDishNutricionalInfo(dish, newInfo);
        Invariants.ensure(dish.nutricionalInfo().equals(newInfo));

        LOGGER.info("»»» Changed nutricional info of {} to {}", dish, dish.nutricionalInfo());
    }

    private void testReportingDish() {
        final Iterable<DishesPerCaloricCategory> l1 = dishReportingController
                .reportDishesPerCaloricCategory();
        Invariants.nonNull(l1);

        final Iterable<Object[]> l2 = dishReportingController
                .reportDishesPerCaloricCategoryAsTuples();
        Invariants.nonNull(l2);

        final Iterable<DishesPerDishType> l3 = dishReportingController.reportDishesPerDishType();
        Invariants.nonNull(l3);

        final Iterable<Dish> l4 = dishReportingController.reportHighCaloriesDishes();
        Invariants.nonNull(l4);

        LOGGER.info("»»» Reporting dishes");
    }

    private void testActivateDeactivateDishType() {
        final Iterable<DishType> l = activateDishTypeController.allDishTypes();
        Invariants.nonNull(l);

        DishType dish = l.iterator().next();

        final boolean oldStatus = dish.isActive();

        dish = activateDishTypeController.changeDishTypeState(dish);
        Invariants.ensure(dish.isActive() == !oldStatus);

        LOGGER.info("»»» Changed status of {} to {}", dish, dish.isActive());
    }
}
