/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.dto;

import eapli.framework.general.domain.model.Money;
import eapli.framework.representations.RepresentationBuilder;

/**
 *
 * @author Paulo Gandra de Sousa
 *
 */
public class DishDTORepresentationBuilder implements RepresentationBuilder<DishDTO> {

    /**
     *
     */
    private static final String PROPERTY = "Property '";

    /**
     *
     */
    private static final String NOT_KNOW_IN_DISH_DTO = "' not know in dishDTO";

    private final DishDTO dto = new DishDTO();

    private String childObject = "";

    @Override
    public DishDTO build() {
        return dto;
    }

    @Override
    public RepresentationBuilder<DishDTO> startObject(final String name) {
        childObject = name;
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> endObject() {
        childObject = "";
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> withProperty(final String name, final String value) {
        if ("name".equals(name)) {
            dto.name = value;
        } else if ("acronym".equals(name) && "dishType".equals(childObject)) {
            dto.dishTypeAcronym = value;
        } else if ("description".equals(name) && "dishType".equals(childObject)) {
            dto.dishTypeDescription = value;
        } else {
            throw new IllegalArgumentException(PROPERTY + name + NOT_KNOW_IN_DISH_DTO);
        }
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> withProperty(final String name, final Integer value) {
        if ("salt".equals(name) && "nutricionalInfo".equals(childObject)) {
            dto.salt = value.intValue();
        } else if ("calories".equals(name) && "nutricionalInfo".equals(childObject)) {
            dto.calories = value.intValue();
        } else {
            throw new IllegalArgumentException(PROPERTY + name + NOT_KNOW_IN_DISH_DTO);
        }
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> withProperty(final String name, final Money value) {
        if ("currentPrice".equals(name)) {
            dto.price = value.amount();
            dto.currency = value.currency().toString();
        } else {
            throw new IllegalArgumentException(PROPERTY + name + NOT_KNOW_IN_DISH_DTO);
        }
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> withProperty(final String name, final Boolean value) {
        if ("active".equals(name)) {
            dto.active = value;
        } else {
            throw new IllegalArgumentException(PROPERTY + name + NOT_KNOW_IN_DISH_DTO);
        }
        return this;
    }

    @Override
    public RepresentationBuilder<DishDTO> withElement(final String value) {
        throw new IllegalArgumentException("DishDTO has no collections");
    }
}
