/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.dishmanagement.domain;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Version;

import eapli.ecafeteria.dishmanagement.dto.DishDTO;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerCaloricCategory;
import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Designation;
import eapli.framework.general.domain.model.Money;
import eapli.framework.representations.RepresentationBuilder;
import eapli.framework.representations.Representationable;
import eapli.framework.representations.dto.DTOable;
import eapli.framework.validations.Preconditions;

/**
 * A Dish
 *
 * @author Jorge Santos ajs@isep.ipp.pt
 *
 */
@Entity
@SqlResultSetMapping(name = "DishesPerCaloricCategoryMapping", classes = @ConstructorResult(targetClass = DishesPerCaloricCategory.class, columns = {
        @ColumnResult(name = "caloricCategory"), @ColumnResult(name = "n") }))
public class Dish implements AggregateRoot<Designation>, DTOable<DishDTO>, Representationable {

    private static final long serialVersionUID = 1L;

    @Version
    private Long version;

    @EmbeddedId
    private Designation name;
    /**
     * cascade = CascadeType.NONE as the dishType is part of another aggregate
     */
    @ManyToOne()
    private DishType dishType;
    private NutricionalInfo nutricionalInfo;
    private Money price;
    private boolean active;

    public Dish(final DishType dishType, final Designation name,
            final NutricionalInfo nutricionalInfo, final Money price) {
        Preconditions.noneNull(dishType, name, nutricionalInfo);

        this.dishType = dishType;
        this.name = name;
        this.nutricionalInfo = nutricionalInfo;
        this.setPrice(price);
        this.active = true;
    }

    public Dish(final DishType dishType, final Designation name, final Money price) {
        Preconditions.noneNull(dishType, name, price);

        this.dishType = dishType;
        this.name = name;
        this.nutricionalInfo = null;
        this.price = price;
        this.active = true;
    }

    protected Dish() {
        // for ORM only
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        if (!(other instanceof Dish)) {
            return false;
        }

        final Dish that = (Dish) other;
        if (this == that) {
            return true;
        }

        return identity().equals(that.identity()) && dishType.equals(that.dishType)
                && nutricionalInfo.equals(that.nutricionalInfo) && price.equals(that.price)
                && active == that.active;
    }

    public DishType dishType() {
        return this.dishType;
    }

    @Override
    public Designation identity() {
        return this.name;
    }

    public NutricionalInfo nutricionalInfo() {
        return this.nutricionalInfo;
    }

    public Designation name() {
        return this.name;
    }

    public Money currentPrice() {
        return this.price;
    }

    /**
     *
     * @return true or false whether is or not active
     */
    public boolean isActive() {
        return this.active;
    }

    /**
     * toggles the state of the dish, activating it or deactivating it
     * accordingly.
     *
     * @return whether the dish is active or not
     */
    public boolean toogleState() {
        this.active = !this.active;
        return isActive();
    }

    /**
     * Changes the nutritional info of the dish
     *
     * @param newNutricionalInfo
     *            The new NutricionalInfo.
     */
    public void changeNutricionalInfoTo(final NutricionalInfo newNutricionalInfo) {
        if (newNutricionalInfo == null) {
            throw new IllegalArgumentException();
        }
        this.nutricionalInfo = newNutricionalInfo;
    }

    public void changePriceTo(final Money newPrice) {
        // TODO extra business logic associated with changing the price of a
        // dish, e.g., save price history
        setPrice(newPrice);
    }

    private void setPrice(final Money price) {
        if (price == null || price.isLessThanOrEqual(Money.euros(0))) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    @Override
    public DishDTO toDTO() {
        return new DishDTO(dishType.identity(),
                dishType.description(),
                name.toString(),
                nutricionalInfo.calories(),
                nutricionalInfo.salt(),
                price.amount(),
                price.currency().getCurrencyCode(),
                active);
    }

    /**
     * Showcase the Representationable interface
     */
    @Override
    public <R> R buildRepresentation(final RepresentationBuilder<R> builder) {
        builder.startObject("Dish")
                .withProperty("name", name)
                .withProperty("currentPrice", currentPrice())
                .withProperty("active", active)
                .startObject("dishType")
                .withProperty("acronym", dishType.identity())
                .withProperty("description", dishType.description())
                .endObject();
        if (nutricionalInfo != null) {
            builder.startObject("nutricionalInfo")
                    .withProperty("salt", nutricionalInfo.salt())
                    .withProperty("calories", nutricionalInfo.calories())
                    .endObject();
        }
        return builder.build();
    }
}
