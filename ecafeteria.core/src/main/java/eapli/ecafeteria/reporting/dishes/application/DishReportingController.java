/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.reporting.dishes.application;

import eapli.ecafeteria.Application;
import eapli.ecafeteria.dishmanagement.domain.Dish;
import eapli.ecafeteria.infrastructure.persistence.PersistenceContext;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerCaloricCategory;
import eapli.ecafeteria.reporting.dishes.dto.DishesPerDishType;
import eapli.ecafeteria.reporting.dishes.repositories.DishReportingRepository;
import eapli.framework.application.UseCaseController;

/**
 * reporting is a separate concern from business logic, as such we are placing
 * reporting features in a separate package structure.
 *
 * @author Paulo Gandra de Sousa
 */
@UseCaseController
public class DishReportingController {

    private final DishReportingRepository repo = PersistenceContext.repositories().dishReporting();

    /**
     * reports the number of dishes per dish type
     *
     * in this case we are using a custom reporting DTO
     *
     * @return
     */
    public Iterable<DishesPerDishType> reportDishesPerDishType() {
        return repo.dishesPerDishType();
    }

    /**
     * reports the dishes that have a high calorie count
     *
     * "high calorie" is defined in the application properties
     *
     * @return
     */
    public Iterable<Dish> reportHighCaloriesDishes() {
        return repo.reportHighCaloriesDishes(Application.settings().getHighCaloriesDishLimit());
    }

    /**
     * reports the number of dishes per caloric group (low, medium, high)
     *
     * in this case we are using a custom DTO
     *
     * @return
     */
    public Iterable<DishesPerCaloricCategory> reportDishesPerCaloricCategory() {
        return repo.reportDishesPerCaloricCategory();
    }

    /**
     * reports the number of dishes per caloric group (low, medium, high)
     *
     * in this case we are returning the data as standard tuples (arrays of
     * objects)
     *
     * @return
     */
    public Iterable<Object[]> reportDishesPerCaloricCategoryAsTuples() {
        return repo.reportDishesPerCaloricCategoryAsTuples();

    }
}
