/*
 * Copyright (c) 2013-2019 the original author or authors.
 *
 * MIT License
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package eapli.ecafeteria.cafeteriausermanagement.domain;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eapli.framework.domain.model.AggregateRoot;
import eapli.framework.domain.model.DomainEntities;
import eapli.framework.general.domain.model.Money;
import eapli.framework.validations.Preconditions;

/**
 * One movement of a user card.
 *
 * CardMovements are aggregates as there can be many movements for each single
 * user. as such, the movement is treated as an aggregate by its own with a back
 * link to the user and not as a part of the user card.
 *
 * @author mcn
 */
@Entity
public class CardMovement implements AggregateRoot<Long> {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue // (strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private MovementType type;
    private Money amount;
    @ManyToOne(fetch = FetchType.LAZY)
    private CafeteriaUser user;
    @Temporal(TemporalType.DATE)
    private Calendar occurredOn;

    protected CardMovement() {
        // for ORM tool only
    }

    public CardMovement(final MovementType type, final Money ammount,
            final CafeteriaUser userCard) {
        this(type, ammount, userCard, Calendar.getInstance());
    }

    public CardMovement(final MovementType type, final Money amount, final CafeteriaUser user,
            final Calendar dateOccurrence) {
        Preconditions.noneNull(type, amount, user, dateOccurrence);

        this.type = type;
        this.amount = amount;
        this.user = user;
        occurredOn = dateOccurrence;
    }

    public CafeteriaUser account() {
        return user;
    }

    public MovementType movementType() {
        return type;
    }

    public Calendar occurredOn() {
        return occurredOn;
    }

    public Money amount() {
        return amount;
    }

    public Money accountingValue() {
        return type == MovementType.PURCHASE ? amount.negate() : amount;
    }

    @Override
    public boolean equals(final Object o) {
        return DomainEntities.areEqual(this, o);
    }

    @Override
    public int hashCode() {
        return DomainEntities.hashCode(this);
    }

    @Override
    public boolean sameAs(final Object other) {
        return DomainEntities.areEqual(this, other);
    }

    @Override
    public Long identity() {
        return id;
    }
}
