/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates and open the template
 * in the editor.
 */
package eapli.ecafeteria.traceability.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 *
 * @author mcn
 */
public class MaterialTest {

    @Test(expected = IllegalArgumentException.class)
    public void ensureAcronymMustNotBeEmpty() {
        System.out.println("must have non-empty acronym");
        new Material("", "");
    }

    @Test(expected = IllegalArgumentException.class)
    public void ensureAcronymMustNotBeNull() {
        System.out.println("must have an acronym");
        new Material(null, "");
    }

    @Test
    public void testChangeDescriptionTo() {
        System.out.println("changeDescriptionTo");
        final Material instance = new Material("eggs", "Eggs (chicken, duck)");
        final String newDescription = "new description";
        instance.changeDescriptionTo(newDescription);
        assertEquals(newDescription, instance.description());
    }

    /**
     * Test of id method, of class DishType.
     */
    @Test
    public void testId() {
        System.out.println("id");
        final String acronym = "eggs";
        final Material instance = new Material(acronym, "real eggs");
        assertEquals(acronym, instance.identity());
    }

    /**
     * Test of is method, of class DishType.
     */
    @Test
    public void testIs() {
        System.out.println("Test is method");
        final String id = "eggs";
        final Material instance = new Material(id, "real eggs");
        assertTrue(instance.hasIdentity(id));
    }
}
